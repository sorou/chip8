# CHIP-8 Emulator

Emulator/interpreter for the [CHIP-8](https://en.wikipedia.org/wiki/CHIP-8) system/programming language, written in Rust using [SFML](https://github.com/JeremyLetang/rust-sfml).

## Usage

### Running a CHIP-8 program
`chip8 [options] example.c8`

In general, you should leave options on their defaults unless you know what you're doing. To view list of options, run `chip8 --help`.

Alternatively, if you don't specify any ROM file, a dialog window will appear, prompting you to select one. Note that only the opcodes up to and including the SUPER-CHIP specification (1991) are supported. Additional opcodes, or opcodes modified after this version, will lead to undefined behavior, up to and including crashes. If you happen upon a CHIP-8 file that doesn't work as expected, feel free to submit an issue!

### Input
The CHIP-8 system uses a hexadecimal input keypad (that is, a keypad with only the keys for decimal digits 0-9, as well as letters A-F). This emulator can be used using these literal keys (e.g. typing a C or a 5 on your literal keyboard will send the corresponding character to the system). Support for more convenient control schemes is in progress. If you happen to have a physical, hexadecimal keypad, it should work with no further configuration. If not, please create an issue! I'd love to hear about your experience.

## License
MIT
