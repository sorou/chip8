use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "chip8", about = "chip8 emulator implemented in rust")]
pub struct Config {
    #[structopt(short = "w", long = "width", help = "Width of the screen, in pixels (1-256)", default_value = "64")] 
    pub screen_width: u8,
    #[structopt(short = "h", long = "height", help = "Height of the screen, in pixels (1-256)", default_value = "32")] 
    pub screen_height: u8,
    #[structopt(short = "m", long = "memory", help = "Amount of memory (RAM and ROM) to use, in bytes (1-65536)", default_value = "4096")] 
    pub ram_size: u16,

    #[structopt(help = "ROM file to run")]
    pub rom_file_name: Option<String>,
}

