use sfml::audio::{SoundBuffer, Sound, SoundStatus};
use sfml::system::SfBox;

const SAMPLE_RATE: usize = 44100;

pub fn beep<'a>(sound: &mut Sound<'a>) {
    if let SoundStatus::Stopped = sound.status() {
        sound.play();
    }
}

pub fn stop<'a>(sound: &mut Sound<'a>) {
    sound.stop();
}

pub fn init_sound_buff() -> SfBox<SoundBuffer> {
    return SoundBuffer::from_samples(
        &gen_samples(),
        1,
        SAMPLE_RATE as u32
    ).unwrap_or_else(|| {
        warn!("Could not load sound buffer");
        SoundBuffer::from_samples(
            &[0i16; SAMPLE_RATE],
            2,
            SAMPLE_RATE as u32
        ).unwrap()
    })
}

// return an array of audio samples of a square wave
// specifically, one second of audio sampled at 44100 per second
// each i16 represents amplitude of the sound wave at that point
fn gen_samples() -> [i16; SAMPLE_RATE as usize] {
    // in hz
    let frequency = 440f64;
    let wave_end = (2f64 * std::f64::consts::PI) * frequency;
    let mut samples = [0i16; SAMPLE_RATE];

    for i in 0..SAMPLE_RATE {
        let pct = (i as f64) / (SAMPLE_RATE as f64);

        // get amplitude at this point as sin
        let sin = (pct * wave_end).sin();
        let square = if sin >= 0f64 { std::i16::MAX } else { std::i16::MIN };
        samples[i] = square;
    }

    return samples;
}

