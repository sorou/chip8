extern crate sfml;

use sfml::window::*;

pub struct Input {
    pub pressed_keys: [u8; 16],
}

impl Input {
    pub fn new() -> Self {
        Input {
            pressed_keys: [0u8; 16],
        }
    }

    // return true if user wants to quit, false otherwise
    // that'll probably get confusing
    pub fn handle_event(&mut self, e: Event) -> bool {
        match e {
            Event::Closed
              | Event::KeyPressed {
                    code: Key::Escape, ..
            } => {
                return true;
            },
            Event::KeyPressed { code, ..} => {
                self.pressed(code);
            },
            Event::KeyReleased { code, ..} => {
                self.released(code);
            },
            _ => {}
        }

        return false;
    }

    fn pressed(&mut self, code: Key) {
        match code {
            Key::Num0 | Key::Numpad0 => {
                self.pressed_keys[0] = 1;
            },
            Key::Num1 | Key::Numpad1 => {
                self.pressed_keys[1] = 1;
            },
            Key::Num2 | Key::Numpad2 => {
                self.pressed_keys[2] = 1;
            },
            Key::Num3 | Key::Numpad3 => {
                self.pressed_keys[3] = 1;
            },
            Key::Num4 | Key::Numpad4 => {
                self.pressed_keys[4] = 1;
            },
            Key::Num5 | Key::Numpad5 => {
                self.pressed_keys[5] = 1;
            },
            Key::Num6 | Key::Numpad6 => {
                self.pressed_keys[6] = 1;
            },
            Key::Num7 | Key::Numpad7 => {
                self.pressed_keys[7] = 1;
            },
            Key::Num8 | Key::Numpad8 => {
                self.pressed_keys[8] = 1;
            },
            Key::Num9 | Key::Numpad9 => {
                self.pressed_keys[9] = 1;
            },
            Key::A => {
                self.pressed_keys[0xA] = 1;
            },
            Key::B => {
                self.pressed_keys[0xB] = 1;
            },
            Key::C => {
                self.pressed_keys[0xC] = 1;
            },
            Key::D => {
                self.pressed_keys[0xD] = 1;
            },
            Key::E => {
                self.pressed_keys[0xE] = 1;
            },
            Key::F => {
                self.pressed_keys[0xF] = 1;
            },
            _ => {}
        }
    }

    fn released(&mut self, code: Key) {
        match code {
            Key::Num0 | Key::Numpad0 => {
                self.pressed_keys[0] = 0;
            },
            Key::Num1 | Key::Numpad1 => {
                self.pressed_keys[1] = 0;
            },
            Key::Num2 | Key::Numpad2 => {
                self.pressed_keys[2] = 0;
            },
            Key::Num3 | Key::Numpad3 => {
                self.pressed_keys[3] = 0;
            },
            Key::Num4 | Key::Numpad4 => {
                self.pressed_keys[4] = 0;
            },
            Key::Num5 | Key::Numpad5 => {
                self.pressed_keys[5] = 0;
            },
            Key::Num6 | Key::Numpad6 => {
                self.pressed_keys[6] = 0;
            },
            Key::Num7 | Key::Numpad7 => {
                self.pressed_keys[7] = 0;
            },
            Key::Num8 | Key::Numpad8 => {
                self.pressed_keys[8] = 0;
            },
            Key::Num9 | Key::Numpad9 => {
                self.pressed_keys[9] = 0;
            },
            Key::A => {
                self.pressed_keys[0xA] = 0;
            },
            Key::B => {
                self.pressed_keys[0xB] = 0;
            },
            Key::C => {
                self.pressed_keys[0xC] = 0;
            },
            Key::D => {
                self.pressed_keys[0xD] = 0;
            },
            Key::E => {
                self.pressed_keys[0xE] = 0;
            },
            Key::F => {
                self.pressed_keys[0xF] = 0;
            },
            _ => {}
        }
    }
}

