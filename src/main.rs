#[macro_use] extern crate log;

mod chip8;
mod config;
mod input;
mod graphics;
mod sound;
mod consts;

use structopt::StructOpt;
use sfml::system::*;
use simplelog::*;

fn main() {
    // we don't really NEED logging for this to work
    // but if it fails to initialize then something is wrong
    TermLogger::init(LevelFilter::Debug, Config::default(), TerminalMode::Mixed)
        .unwrap_or_else(|_| eprintln!("Failed to initialize logger. That's probably bad."));

    let conf = config::Config::from_args();

    debug!("{:?}", conf);

    let mut input = input::Input::new();

    let mut graphics = graphics::Graphics::new(&conf);
    let buff = sound::init_sound_buff();
    let mut sound = sfml::audio::Sound::with_buffer(&buff);
    let mut chip8 = chip8::Chip8::new(&conf);

    info!("CPU initialized");

    let mut clock = Clock::start();
    let mut frametimer = 0.0;
    let mut framecount = 0;

    loop {
        let delta = clock.restart().as_seconds();

        while let Some(e) = graphics.poll_event() {
            let quit = input.handle_event(e);
            if quit {
                graphics.close();
                return;
            }
        }

        chip8.step(delta, &input.pressed_keys);

        if chip8.draw {
            graphics.draw(&chip8.screen);
        }

        // spec says chip8 sound timer needs to be at least 2 to respond
        if chip8.sound_timer > 1 {
            sound::beep(&mut sound);
        } else {
            sound::stop(&mut sound);
        }

        frametimer += delta;
        framecount += 1;

        if frametimer >= 1.0 {
            frametimer = 0.0;

            // this is a hack to keep fps on one line
            debug!("FPS: {}", framecount);
            framecount = 0;
        }
    }
}

