use crate::config;
use crate::consts;

use rand::Rng;
use std::vec::Vec;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use tinyfiledialogs::open_file_dialog;

#[derive(Debug)]
pub struct Chip8 {
    reg: [u8; 16],
    mem: Vec<u8>,
    pub screen: Vec<Vec<u8>>,
    // main should query this each frame to know whether to update the display
    pub draw: bool,
    // set by opcode FX0A
    pub waiting_input: bool,
    waiting_reg: usize,

    index: u16,
    pc: u16,

    // seconds since timers were last updated
    dt: f32,
    delay_timer: u8,
    pub sound_timer: u8,

    stack: [u16; 16],
    // this is really just here for legacy reasons
    // using Vec for stack means we don't manage sp ourselves
    sp: u16,

    keys: [u8; 16],
}

impl Chip8 {
    pub fn new(conf: &config::Config) -> Self {
        let reg = [0u8; 16];
        let mut mem = vec![0u8; conf.ram_size.into()];
        let screen = vec![vec![0u8; conf.screen_width.into()]; conf.screen_height.into()];
        let stack = [0u16; 16];
        let key = [0u8; 16];
        let pc = 0x200;

        // load font set into memory
        for i in 0..consts::FONT.len() {
            mem[i + 0x50] = consts::FONT[i];
        }

        // get file name from config, or ask user for a literal file
        let file_name = if let Some(name) = &conf.rom_file_name {
            name.clone()
        } else {
            if let Some(n) = open_file_dialog("Load a Chip8 ROM file...", "", Some((&["*.c8", "*.ch8", "*.chip8"], "Chip-8 ROM files"))) {
                n
            } else {
                panic!("Please select a file to load.");
            }
        };

        // load program into memory
        let mut file = if let Ok(f) = File::open(PathBuf::from(&file_name)) {
            f
        } else {
            panic!("Could not open ROM file {} for reading. Aborting.", file_name);
        };

        if let Ok(n) = file.read(&mut mem[pc..]) {
            debug!("Loaded ROM file {} containing {} bytes into memory.", file_name, n);
        } else {
            panic!("Failed to read bytes from ROM file {}. Aborting.", file_name);
        }

        Chip8 {
            reg: reg,
            mem: mem,
            screen: screen,
            draw: false,
            waiting_input: false,
            waiting_reg: 0,

            index: 0,
            pc: pc as u16,

            dt: 0f32,
            delay_timer: 0,
            sound_timer: 0,

            stack: stack,
            sp: 0,

            keys: key,
        }
    }

    pub fn step(&mut self, delta: f32, keys: &[u8; 16]) {
        self.draw = false;

        // load current state of keyboard
        for i in 0..self.keys.len() {
            self.keys[i] = keys[i];
        }

        if !self.waiting_input {
            // each instruction is 2 bytes
            if self.pc >= (self.mem.len() - 1) as u16 {
                panic!("Reached end of memory during execution. Aborting.");
            }

            let next_inst = self.fetch_next();
            self.run(next_inst);

            self.pc += consts::OPCODE_SIZE;
        } else {
            self.check_key();
        }

        // timers should be independent of CPU activity
        self.dt += delta;
        if self.dt >= consts::FRAME_TIME_SECONDS {
            self.update_timers();
            self.dt = 0f32;
        }
    }

    fn fetch_next(&self) -> u16 {
        // need to cast these u16 since otherwise we're left-shifting a u8 8 times, which probably
        // isn't good.
        return (self.mem[self.pc as usize] as u16) << 8
            | self.mem[(self.pc as usize) + 1] as u16;
    }

    fn run(&mut self, inst: u16) {
        // divide opcode into groups of 4 bits
        // makes it easier to do matching on later
        let nibs = (
            (inst & 0xF000) >> 12 as u8,
            (inst & 0x0F00) >> 8 as u8,
            (inst & 0x00F0) >> 4 as u8,
            (inst & 0x000F) as u8
        );

        let addr = (inst & 0x0FFF) as usize;
        let imm4 = ((inst & 0x00F0) >> 4) as u8;
        let imm8 = (inst & 0x00FF) as u8;
        let reg1 = ((inst & 0x0F00) >> 8) as usize;
        // nib 3 can be either a 4-bit literal or a register depending on the instruction
        let reg2 = imm4 as usize;

        match nibs {
            (0x00, 0x00, 0x0E, 0x00) => self.clear(),
            (0x00, 0x00, 0x0E, 0x0E) => self.ret(),
            (0x01, _, _, _) => self.jump(addr),
            (0x02, _, _, _) => self.call(addr),
            (0x03, _, _, _) => self.jeqi(reg1, imm8),
            (0x04, _, _, _) => self.jnei(reg1, imm8),
            (0x05, _, _, 0x00) => self.jeq(reg1, reg2),
            (0x06, _, _, _) => self.loadi(reg1, imm8),
            (0x07, _, _, _) => self.addi(reg1, imm8),
            (0x08, _, _, 0x00) => self.load(reg1, reg2),
            (0x08, _, _, 0x01) => self.or(reg1, reg2),
            (0x08, _, _, 0x02) => self.and(reg1, reg2),
            (0x08, _, _, 0x03) => self.xor(reg1, reg2),
            (0x08, _, _, 0x04) => self.add(reg1, reg2),
            (0x08, _, _, 0x05) => self.sub(reg1, reg2),
            (0x08, _, _, 0x06) => self.shr(reg1),
            (0x08, _, _, 0x07) => self.subneg(reg1, reg2),
            (0x08, _, _, 0x0E) => self.shl(reg1),
            (0x09, _, _, 0x00) => self.jne(reg1, reg2),
            (0x0A, _, _, _) => self.loadaddr(addr),
            (0x0B, _, _, _) => self.jmpoffset(addr),
            (0x0C, _, _, _) => self.loadrand(reg1, imm8),
            (0x0D, _, _, _) => self.disp(reg1, reg2, nibs.3),
            (0x0E, _, 0x09, 0x0E) => self.jmpkey(reg1),
            (0x0E, _, 0x0A, 0x01) => self.jmpnkey(reg1),
            (0x0F, _, 0x00, 0x07) => self.getdelay(reg1),
            (0x0F, _, 0x00, 0x0A) => self.wait_keypress(reg1),
            (0x0F, _, 0x01, 0x05) => self.setdelay(reg1),
            (0x0F, _, 0x01, 0x08) => self.setsound(reg1),
            (0x0F, _, 0x01, 0x0E) => self.addindex(reg1),
            (0x0F, _, 0x02, 0x09) => self.loadchar(reg1),
            (0x0F, _, 0x03, 0x03) => self.bcd(reg1),
            (0x0F, _, 0x05, 0x05) => self.regdump(reg1),
            (0x0F, _, 0x06, 0x05) => self.regload(reg1),
            _ => {
                panic!("Unsupported instruction {}. Aborting", inst);
            }
        };

    }

    // check if a key is pressed
    // if so, write its value to the register we're waiting to populate
    // and stop waiting for input
    fn check_key(&mut self) {
        for i in 0..self.keys.len() {
            if self.keys[i] != 0 {
                self.reg[self.waiting_reg] = i as u8;
                self.waiting_input = false;
            }
        }
    }

    fn update_timers(&mut self) {
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }

        if self.sound_timer > 0 {
            self.sound_timer -= 1;
        } 
    }

    // 0x00E0
    // clear the screen
    fn clear(&mut self) {
        for i in 0..self.screen.len() {
            for j in 0..self.screen[i].len() {
                self.screen[i][j] = 0;
            }
        }

        self.draw = true;
    }

    // 0x00EE
    // return from subroutine
    fn ret(&mut self) {
        if self.stack.len() == 0 {
            panic!("Attempted to return from a subroutine, but call stack is empty. Aborting. Call occurred at address {}", self.pc);
        }

        self.sp -= 1;
        let addr = self.stack[self.sp as usize];
        self.pc = addr;
    }

    // 0x01NNN
    // jump to address
    fn jump(&mut self, addr: usize) {
        self.pc = addr as u16;
        // addr is the address of the next instruction that should be run
        // so we actually set the pc to the address of the instruction before that
        // since, after this instruction finishes, the pc will be incremented to point to the "next"
        // instruction, which is actually the one at address addr
        self.pc -= consts::OPCODE_SIZE;
    }

    // 0x2NNN
    // call the subroutine at address
    fn call(&mut self, address: usize) {
        self.stack[self.sp as usize] = self.pc;
        self.sp += 1;
        self.pc = address as u16;
        self.pc -= consts::OPCODE_SIZE;
    }

    // 0x3XNN
    // skip the next instruction if value in register r1 == imm8
    fn jeqi(&mut self, r1: usize, imm8: u8) {
        if self.reg[r1] == imm8 { self.pc += consts::OPCODE_SIZE; }
    }

    // 0x4xNN
    // skip the next instruction if value in register r1 != imm8
    fn jnei(&mut self, r1: usize, imm8: u8) {
        if self.reg[r1] != imm8 { self.pc += consts::OPCODE_SIZE; }
    }

    // 0x5XY0
    // skip next instruction if value in register r1 == value in register r2
    fn jeq(&mut self, r1: usize, r2: usize) {
        if self.reg[r1] == self.reg[r2] { self.pc += consts::OPCODE_SIZE; }
    }

    // 0x6XNN
    // assign imm8 to r1
    fn loadi(&mut self, r1: usize, imm8: u8) {
        self.reg[r1] = imm8;
    }

    // 0x7XNN
    // add imm8 to r1 and store the value in r1
    fn addi(&mut self, r1: usize, imm8: u8) {
        self.reg[r1] = self.reg[r1].wrapping_add(imm8);
    }

    // 0x8XY0
    // set r1 to value in r2
    fn load(&mut self, r1: usize, r2: usize) {
        self.reg[r1] = self.reg[r2];
    }

    // 0x8XY1
    // set r1 to (r1 | r2)
    fn or(&mut self, r1: usize, r2: usize) {
        self.reg[r1] |= self.reg[r2];
    }

    // 0x8XY2
    // set r1 to (r1 & r2)
    fn and(&mut self, r1: usize, r2: usize) {
        self.reg[r1] &= self.reg[r2];
    }

    // 0x8XY3
    // set r1 to (r1 ^ r2)
    fn xor(&mut self, r1: usize, r2: usize) {
        self.reg[r1] ^= self.reg[r2];
    }

    // 0x8XY4
    // set r1 to (r1 + r2), set reg[0xF] to 1 if a carry occurred, 0 otherwise
    fn add(&mut self, r1: usize, r2: usize) {
        let oldval = self.reg[r1];
        self.reg[r1] = self.reg[r1].wrapping_add(self.reg[r2]);

        // if it's smaller than its old value after an addition, there must have been overflow
        // set the carry bit
        self.reg[0xF] = if self.reg[r1] < oldval {
            1
        } else {
            0
        }
    }

    // 0x8XY5
    // set r1 to (r1 - r2), set reg[0xF] to 0 if a borrow occurred, 1 otherwise
    fn sub(&mut self, r1: usize, r2: usize) {
        // we'll have to borrow no matter what if r1 < r2
        self.reg[0xF] = if self.reg[r1] < self.reg[r2] {
            0
        } else {
            1
        };
        
        // wrapping_sub guards against under/overflow
        self.reg[r1] = self.reg[r1].wrapping_sub(self.reg[r2]);
    }

    // 0x8XY6
    // set r1 to (r1 >> 1), after storing its least significant bit in reg[0xF]
    fn shr(&mut self, r1: usize) {
        self.reg[0xF] = self.reg[r1] & 0x01;

        self.reg[r1] >>= 1;
    }

    // 0x8XY7
    // set r1 to (r2 - r1), set reg[0xF] to 0 if a borrow occurred, 1 otherwise
    fn subneg(&mut self, r1: usize, r2: usize) {
        self.reg[0xF] = if self.reg[r2] < self.reg[r1] {
            0
        } else {
            1
        };

        self.reg[r1] = self.reg[r2].wrapping_sub(self.reg[r1]);
    }

    // 0x8XYE
    // set r1 to (r1 << 1), after storing its most significant bit in reg[0xF]
    fn shl(&mut self, r1: usize) {
        self.reg[0xF] = self.reg[r1] & 0x80;
        // user expects 1 if MSB of r1 was 1, 0 otherwise. 
        // right now we stored 0x80 or 0, so shift to turn 0x80 into a 1
        self.reg[0xF] >>= 7;

        self.reg[r1] <<= 1;
    }

    // 0x9XY0
    // skip next instruction if r1 != r2
    fn jne(&mut self, r1: usize, r2: usize) {
        // point pc at next instruction so that when opcode exec
        // finishes, it'll be advanced to the instruction after that
        self.pc += if self.reg[r1] != self.reg[r2] {
           consts::OPCODE_SIZE
        } else {
            0
        }
    }

    // 0xANNN
    // set index to address NNN
    fn loadaddr(&mut self, address: usize) {
        self.index = address as u16;
    }

    // 0xBNNN
    // jump to instruction at address NNN + reg[0]
    fn jmpoffset(&mut self, address: usize) {
        let offaddr = address + (self.reg[0] as usize);
        self.pc = offaddr as u16;
        self.pc -= consts::OPCODE_SIZE;
    }

    // 0xCXNN
    // set r1 to (randomValue & NN)
    fn loadrand(&mut self, r1: usize, imm8: u8) {
        let mut rng = rand::thread_rng();
        self.reg[r1] = (rng.gen::<u8>()) & imm8;

    }

    // 0xDXYN
    // draw a sprite at coordinate (reg[r1], reg[r2]) with width of 8 px
    // and height of imm8 px. the pixels to be drawn are taken from the
    // memory location starting at index. specifically, we read one byte
    // (each bit is one px -> 8 px) at a time from memory starting at index
    // we do this imm8 times to draw an 8x(imm8) px sprite
    // draw by xor'ing each pixel with its current value in graphics memory
    // set reg[0xF] to 1 if any pixels changed from 1 to 0, and set it to 0
    // otherwise. Apparently, this is used for collision detection. I don't
    // know; I'm just implementing the spec
    fn disp(&mut self, r1: usize, r2: usize, imm8: u8) {
        let startx = self.reg[r1] as usize;
        let starty = self.reg[r2] as usize;

        self.reg[0xF] = 0;

        for row in 0..imm8 as usize {
            // modulo because sometimes a program will try to write outside the screen bounds
            // in the interest of not crashing when this happens, we just have it write to the
            // wrong spot
            let y = (starty + row) % self.screen.len();
            let byte = self.mem[(self.index as usize) + row];
            for col in 0..8 as usize {
                let x = (startx + col) % self.screen[y].len();
                // get the individual bit that we're drawing
                // we set all but the lsb to 0 manually since i'm not totally
                // convinced rust's right shift will be logical here instead of
                // arithmetic. we want logical.
                let bit = (byte >> (7 - col)) & 0x01;

                if y >= self.screen.len() || x >= self.screen[y].len() {
                    warn!("Attempted to draw offscreen at {}, {}. Instruction was disp({}, {}, {})\n{:?}", x, y, r1, r2, imm8, &self);
                }

                // if reg is already set, leave it set
                // otherwise, set it if both the bit we're drawing and
                // the location we're drawing it to are 1, since that 
                // means the location we're drawing it to is about to become 0
                // we don't need to do any crazy shifting or anything on the 
                // screen value since bit is already all 0s except possibly
                // its lsb, so we can only get 1 or 0 from that bitwise and
                self.reg[0xF] |= bit & self.screen[y][x];
                self.screen[y][x] ^= bit;
            }
        }

        // the whole point of this instruction is to change the screen
        self.draw = true;
    }

    // 0xEX9E
    // jump if key stored in r1 is pressed
    fn jmpkey(&mut self, r1: usize) {
        let key = self.reg[r1] as usize;

        self.pc += if self.keys[key] != 0 {
            consts::OPCODE_SIZE
        } else {
            0
        }
    }

    // 0xEXA1
    // jump if key stored in r1 is NOT pressed
    fn jmpnkey(&mut self, r1: usize) {
        let key = self.reg[r1] as usize;

        self.pc += if self.keys[key] == 0 {
            consts::OPCODE_SIZE
        } else {
            0
        }
    }

    // 0xFX07
    // set r1 to whatever the delay timer currently is
    fn getdelay(&mut self, r1: usize) {
        self.reg[r1] = self.delay_timer;
    }

    // 0xFX0A
    // block until a key is pressed, then store the key in r1
    fn wait_keypress(&mut self, r1: usize) {
        self.waiting_input = true;
        self.waiting_reg = r1;
    }

    // 0xFX15
    // set delay timer to value in r1
    fn setdelay(&mut self, r1: usize) {
        self.delay_timer = self.reg[r1];
    }

    // 0xFX18
    // set sound timer to value in r1
    fn setsound(&mut self, r1: usize) {
        self.sound_timer = self.reg[r1];
    }

    // 0xFX1E
    // add value in r1 to index
    fn addindex(&mut self, r1: usize) {
        self.index = self.index.wrapping_add(self.reg[r1] as u16);
    }

    // 0xFX29
    // set index to location of sprite for character corresponding to r1
    fn loadchar(&mut self, r1: usize) {
        let c = self.reg[r1];

        // we load characters into memory starting at 0x50
        // each character is 5 bytes
        // so the address for the first byte of a given character is
        // 0x50 + (5*char)
        self.index = (0x50 + (5 * c)) as u16;
    }

    // 0xFX33
    // store bcd representation of value in r1 to memory at index
    // I don't really know what that means
    // it seems like convert r1 into decimal and store its digits 
    // in memory starting at index, so I'm moving forward with that
    // a u8 can only be up to 256, so 3 digits work fine
    fn bcd(&mut self, r1: usize) {
        let d1 = self.reg[r1] / 100;
        let d2 = (self.reg[r1] % 100) / 10;
        let d3 = self.reg[r1] % 10;

        self.mem[self.index as usize] = d1;
        self.mem[self.index as usize + 1] = d2;
        self.mem[self.index as usize + 2] = d3;
    }

    // 0xFX55
    // dump registers 0 up to r1 (inclusive) to memory starting at address index
    fn regdump(&mut self, r1: usize) {
        for i in 0..=r1 {
            self.mem[self.index as usize + i] = self.reg[i];
        }
    }

    // 0xFX65
    // fill registers 0 up to r1 (inclusive) with values from memory
    // starting at address index
    fn regload(&mut self, r1: usize) {
        for i in 0..=r1 {
            self.reg[i] = self.mem[self.index as usize + i];
        }
    }
}

