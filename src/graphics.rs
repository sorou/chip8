use crate::consts;
use crate::config;

use sfml::graphics::*;
use sfml::window::*;
use sfml::system::*;

pub struct Graphics {
    window: RenderWindow,
}

impl Graphics {
    pub fn new(conf: &config::Config) -> Self {
        let mut window = RenderWindow::new(
            ((conf.screen_width as u32) * consts::SCALE, (conf.screen_height as u32) * consts::SCALE),
            "CHIP-8",
            Style::CLOSE,
            &Default::default(),
        );

        window.set_vertical_sync_enabled(true);
        window.clear(Color::BLACK);
        window.display();

        Graphics { window }
    }

    // get events from active window
    pub fn poll_event(&mut self) -> Option<Event> {
        return self.window.poll_event();
    }

    pub fn draw(&mut self, vram: &Vec<Vec<u8>>) {
        self.window.clear(Color::BLACK);

        for row in 0..vram.len() {
            for col in 0..vram[row].len() {
                if vram[row][col] == 1 {
                    // draw 16x16 rectangle at (col, row)
                    let mut px = RectangleShape::with_size(Vector2f::new(16f32, 16f32));
                    px.set_position(Vector2f::new(col as f32 * 16f32, row as f32 * 16f32));
                    self.window.draw(&px);
                }
            }
        }

        self.window.display();
    }

    pub fn close(&mut self) {
        self.window.close();
    }
}
